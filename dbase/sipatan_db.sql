/*
Navicat MySQL Data Transfer

Source Server         : hanz
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : sipatan_db

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-07-20 19:08:55
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `asn_prokopim`
-- ----------------------------
DROP TABLE IF EXISTS `asn_prokopim`;
CREATE TABLE `asn_prokopim` (
  `id_prokopim` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `nama` text NOT NULL,
  `jabatan` text NOT NULL,
  PRIMARY KEY (`id_prokopim`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of asn_prokopim
-- ----------------------------
INSERT INTO `asn_prokopim` VALUES ('2', '198509132003121002', 'DONI ANDRIAWAN,S.STP.,ME', 'KEPALA BAGIAN PROTOKOL DAN KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('3', '199203032015071001', 'MARSELUS DEDI,S.STP.,M.Sos', 'KEPALA SUB BAGIAN PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('4', '199307062015072002', 'WULANDARI,S.STP', 'ANALIS KEBIJAKAN AHLI MUDA');
INSERT INTO `asn_prokopim` VALUES ('5', '198505232010011005', 'ARIES PRATAMA,S.Kom.,MM', 'ANALIS KEBIJAKAN AHLI MUDA');
INSERT INTO `asn_prokopim` VALUES ('6', '197505262002121009', 'WAWAN HERMAWANSYAH,SE', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('7', '198606012014072002', 'LISA ARIYANTIS.Pd', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('8', '198310022009031005', 'M. YANI,S.I.P.', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('9', '-', 'HERI', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('10', '-', 'YULIADI NINGSIH,S.Pd', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('11', '-', 'SURYANINGSIH,S.Pd', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('12', '-', 'JERRY SUBANDI,SH', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('13', '-', 'M.WAFDA EKI ENANDA', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('14', '-', 'RISTA', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('15', '-', 'SUHERMAN,S.Pd.I', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('16', '-', 'DEBY MARISKA', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('17', '-', 'BAMBANG KURNIAWAN,S.I.P.', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('18', '-', 'BASILIUS YANCE P.,S.Kom', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('19', '-', 'SARTIKA M. MANDAGI M,S.Akun', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('20', '-', 'RANDU KURNIADI', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('21', '-', 'TENI HARLIANTI', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('22', '-', 'LUSIA,S.Pd', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('23', '-', 'A.CHARLES BELLO', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('24', '-', 'HENDRA DWI CAHYADI,SE', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('25', '-', 'HENDRI', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('26', '-', 'MUTYA YUNKE', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('27', '-', 'NIKOLAUS JEPRIANTO,S.M.', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('28', '-', 'TRI FLOREN DONI,S.Kom', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('29', '-', 'WINDA', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('30', '-', 'CHNTIA', 'STAF PROTOKOL');
INSERT INTO `asn_prokopim` VALUES ('31', '198401102010011005', 'SLAMET HADI,S.ST', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('32', '-', 'HAMIDI', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('33', '-', 'ASTRIYULIDA,S.A.P', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('34', '-', 'MIRANTI YUTICA', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('35', '-', 'UTIN ETI DARMANTI', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('36', '-', 'AMINULLAH', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('37', '-', 'DYAR CAHYA GUMILAR', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('38', '-', 'M. FIRMAN MANGAR', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('39', '-', 'HERVIANA MASRANI,S.Kom', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('40', '-', 'ELA PRIANTI', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('41', '-', 'SEPTYA LARASATI', 'STAF KOMUNIKASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('42', '198605262010012003', 'FARIANTINI', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('43', '197307232012121001', 'ALWIADI', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('44', '-', 'UTIN HESTIANA', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('45', '-', 'HENDRY HANDAYANI,SE', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('46', '-', 'EDY SAFITRA', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('47', '-', 'HERI PRIYOKO', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('48', '-', 'YOGI PRATAMA PUTRA', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('49', '-', 'YOPI DAMEL', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('50', '-', 'DARUL HADI', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('51', '-', 'SIRILUS HENGKY', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('52', '-', 'EMERALDA', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('53', '-', 'WILDAN', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('54', '-', 'HILMI KAMAL (UPAN)', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('55', '-', 'RAHMAD DENNY SETIADI', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('56', '-', 'FERRY MEIRUSADI', 'STAF DOKUMENTASI PIMPINAN');
INSERT INTO `asn_prokopim` VALUES ('57', '-', 'DENNI HIDAYAT', 'STAF DOKUMENTASI PIMPINAN');

-- ----------------------------
-- Table structure for `auto_text`
-- ----------------------------
DROP TABLE IF EXISTS `auto_text`;
CREATE TABLE `auto_text` (
  `id_auto_text` int(11) NOT NULL AUTO_INCREMENT,
  `isi_text` text NOT NULL,
  PRIMARY KEY (`id_auto_text`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auto_text
-- ----------------------------

-- ----------------------------
-- Table structure for `disposisi`
-- ----------------------------
DROP TABLE IF EXISTS `disposisi`;
CREATE TABLE `disposisi` (
  `id_disposisi` int(11) NOT NULL AUTO_INCREMENT,
  `surat_opd` int(11) NOT NULL,
  `disposisikan` varchar(100) NOT NULL,
  `note_disposisi` varchar(100) NOT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`id_disposisi`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of disposisi
-- ----------------------------
INSERT INTO `disposisi` VALUES ('2', '83', '196307191996031003', '', '3');
INSERT INTO `disposisi` VALUES ('3', '83', '1', '', '10');
INSERT INTO `disposisi` VALUES ('5', '83', '197908021998021001', '', '5');
INSERT INTO `disposisi` VALUES ('6', '83', '196211241988101002', '', '7');
INSERT INTO `disposisi` VALUES ('9', '0', '196211241988101002', '', '6');

-- ----------------------------
-- Table structure for `opd`
-- ----------------------------
DROP TABLE IF EXISTS `opd`;
CREATE TABLE `opd` (
  `id_opd` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(30) DEFAULT NULL,
  `nama` text,
  `gol` varchar(7) DEFAULT NULL,
  `tmt` date DEFAULT NULL,
  `eslon` varchar(7) DEFAULT NULL,
  `jns_jabatan` varchar(100) DEFAULT NULL,
  `jabatan` text,
  `tmt_jabatan` date DEFAULT NULL,
  `unor_induk` text,
  PRIMARY KEY (`id_opd`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of opd
-- ----------------------------
INSERT INTO `opd` VALUES ('1', '197908021998021001', 'ALEXANDER WILYO, S.STP., M.Si.', 'IV/b', '2019-04-01', 'II.a', 'Jabatan Struktural', 'SEKRETARIS DAERAH', '2021-08-27', 'SEKRETARIAT DAERAH');
INSERT INTO `opd` VALUES ('2', '196807311993121001', 'Drs., SUGIARTO,  M.A.P.', 'IV/c', '2022-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA', '2022-01-11', 'BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA');
INSERT INTO `opd` VALUES ('3', '197212141991081001', 'ANDREAS HARDI, M.Pd.', 'IV/b', '2019-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA BADAN KESATUAN BANGSA DAN POLITIK', '2021-10-08', 'BADAN KESATUAN BANGSA DAN POLITIK');
INSERT INTO `opd` VALUES ('4', '197806081998101001', 'YUNIFAR PURWANTORO, S.STP,M.M.', 'IV/b', '2018-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA PELAKSANA  BADAN PENANGGULANGAN BENCANA DAERAH', '2021-10-08', 'BADAN PENANGGULANGAN BENCANA DAERAH');
INSERT INTO `opd` VALUES ('5', '197004111990031004', 'Drs, PELEALU DEVIE FRANTITO, MM.', 'IV/c', '2015-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA BADAN PENDAPATAN DAERAH', '2021-11-08', 'BADAN PENDAPATAN DAERAH');
INSERT INTO `opd` VALUES ('6', '196503151994121002', 'Drs Drs, H JAHILIN,   M.Pd', 'IV/c', '2015-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA BADAN PENELITIAN DAN PENGEMBANGAN', '2022-01-11', 'BADAN PENELITIAN DAN PENGEMBANGAN');
INSERT INTO `opd` VALUES ('7', '197412121993111001', 'DONATUS FRANSEDA, AP., MM., ', 'IV/c', '2017-10-01', 'II.b', 'Jabatan Struktural', 'KEPALA BADAN PENGELOLA KEUANGAN DAN ASET DAERAH', '2021-11-08', 'BADAN PENGELOLA KEUANGAN DAN ASET DAERAH');
INSERT INTO `opd` VALUES ('8', '196706121996041001', 'HARTO, SE., M.Si', 'IV/c', '2018-10-01', 'II.b', 'Jabatan Struktural', 'KEPALA BADAN PERENCANAAN PEMBANGUNAN DAERAH', '2018-05-09', 'BADAN PERENCANAAN PEMBANGUNAN DAERAH');
INSERT INTO `opd` VALUES ('9', '196601121993021003', 'Drs, YULIANUS, M.A.P.', 'IV/c', '2018-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS KEARSIPAN DAN PERPUSTAKAAN DAERAH', '2022-01-11', 'DINAS KEARSIPAN DAN PERPUSTAKAAN DAERAH');
INSERT INTO `opd` VALUES ('10', '196508151999031003', 'DERSI, SH, M.A.P.', 'IV/c', '2021-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS KEPENDUDUKAN DAN PENCATATAN  SIPIL', '2022-01-11', 'DINAS KEPENDUDUKAN DAN PENCATATAN  SIPIL');
INSERT INTO `opd` VALUES ('11', '196305121985111003', 'RUSTAMI,  S.KM. M.Kes', 'IV/c', '2021-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS KESEHATAN', '2019-10-28', 'DINAS KESEHATAN');
INSERT INTO `opd` VALUES ('12', '197110231991011001', 'Drs, HERYANDI, M.Si.', 'IV/c', '2020-10-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS KETAHANAN PANGAN DAN PERIKANAN', '2022-01-11', 'DINAS KETAHANAN PANGAN DAN PERIKANAN');
INSERT INTO `opd` VALUES ('13', '196408281992031016', 'Drs, NUGROHO WIDYO SISTANTO, M.Si.', 'IV/c', '2020-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS KOMUNIKASI DAN INFORMATIKA', '2021-11-08', 'DINAS KOMUNIKASI DAN INFORMATIKA');
INSERT INTO `opd` VALUES ('14', '196510091992031012', 'Ir., ADI MULIA, M.Hut', 'IV/c', '2022-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS KOPERASI, USAHA KECIL DAN  MENENGAH, PERDAGANGAN DAN PERINDUSTRIAN', '2021-10-08', 'DINAS KOPERASI, USAHA KECIL DAN MENENGAH, PERDAGANGAN DAN PERINDUSTRIAN');
INSERT INTO `opd` VALUES ('15', '196805011995031006', 'ABSALON, SE', 'IV/c', '2021-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PARIWISATA DAN KEBUDAYAAN', '2022-01-11', 'DINAS PARIWISATA DAN KEBUDAYAAN');
INSERT INTO `opd` VALUES ('16', '196809031995081001', 'DENNERY, ST, MT', 'IV/b', '2021-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PEKERJAAN UMUM DAN TATA RUANG', '2022-01-11', 'DINAS PEKERJAAN UMUM DAN TATA RUANG');
INSERT INTO `opd` VALUES ('17', '196507051992031018', 'MANSEN, SH., MH.', 'IV/c', '2017-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PEMBERDAYAAN MASYARAKAT DAN PEMERINTAHAN DESA', '2022-01-11', 'DINAS PEMBERDAYAAN MASYARAKAT DAN PEMERINTAHAN DESA');
INSERT INTO `opd` VALUES ('18', '196904071994121005', 'Drs., SATUKI,  M.Si', 'IV/b', '2020-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PEMUDA DAN OLAHRAGA', '2022-01-11', 'DINAS PEMUDA DAN OLAHRAGA');
INSERT INTO `opd` VALUES ('19', '196704272000031003', 'Drs., MARWAN NOR, MM', 'IV/c', '2021-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU', '2021-11-08', 'DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU');
INSERT INTO `opd` VALUES ('20', '196809101991031007', 'Dr, UCUP SUPRIATNA, S Pd., M.Pd ', 'IV/c', '2021-10-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PENDIDIKAN', '2022-01-11', 'DINAS PENDIDIKAN');
INSERT INTO `opd` VALUES ('21', '196602261993031003', 'AKIA, SE., M.A.P', 'IV/b', '2019-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PERHUBUNGAN', '2021-11-08', 'DINAS PERHUBUNGAN');
INSERT INTO `opd` VALUES ('22', '196612251994031010', 'Ir, SIKAT, M.Si.', 'IV/c', '2015-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PERTANIAN, PETERNAKAN DAN PERKEBUNAN', '2021-05-18', 'DINAS PERTANIAN, PETERNAKAN DAN PERKEBUNAN');
INSERT INTO `opd` VALUES ('23', '196407051992031009', 'Ir, HUSNAN, MTP', 'IV/c', '2020-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS PERUMAHAN RAKYAT, KAWASAN PERMUKIMAN DAN  LINGKUNGAN HIDUP', '2022-01-11', 'DINAS PERUMAHAN RAKYAT, KAWASAN PERMUKIMAN DAN  LINGKUNGAN HIDUP');
INSERT INTO `opd` VALUES ('24', '197511152000122003', 'ALBERTIN TRI KURNIASIH, S.SI, APT', 'IV/b', '2022-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS SOSIAL, PEMBERDAYAAN PEREMPUAN, PERLINDUNGAN ANAK, DAN KB', '2022-01-11', 'DINAS SOSIAL, PEMBERDAYAAN PEREMPUAN, PERLINDUNGAN ANAK, DAN KB');
INSERT INTO `opd` VALUES ('25', '196309191990031007', 'Ir, SUKIRNO', 'IV/c', '2017-10-01', 'II.b', 'Jabatan Struktural', 'KEPALA DINAS TENAGA KERJA DAN TRANSMIGRASI', '2022-01-11', 'DINAS TENAGA KERJA DAN TRANSMIGRASI');
INSERT INTO `opd` VALUES ('26', '196904091990031005', 'REPALIANTO, S.Sos., M.Si.', 'IV/c', '2017-04-01', 'II.b', 'Jabatan Struktural', 'INSPEKTUR', '2021-11-08', 'INSPEKTORAT');
INSERT INTO `opd` VALUES ('27', '196312121984031009', 'MUSLIMIN, S. IP', 'IV/c', '2014-04-01', 'II.b', 'Jabatan Struktural', 'KEPALA SATUAN POLISI PAMONG PRAJA', '2018-01-04', 'SATUAN POLISI PAMONG PRAJA');
INSERT INTO `opd` VALUES ('28', '196608231994031008', 'AGUS HENDRI, SE, M.Si', 'IV/c', '2015-10-01', 'II.b', 'Jabatan Struktural', 'SEKRETARIS DPRD', '2022-01-11', 'SEKRETARIAT DPRD');

-- ----------------------------
-- Table structure for `pejabat`
-- ----------------------------
DROP TABLE IF EXISTS `pejabat`;
CREATE TABLE `pejabat` (
  `id_pejabat` int(11) NOT NULL AUTO_INCREMENT,
  `nip` varchar(50) NOT NULL,
  `nama` text NOT NULL,
  `jabatan` text NOT NULL,
  `ditujukan` int(11) NOT NULL,
  PRIMARY KEY (`id_pejabat`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pejabat
-- ----------------------------
INSERT INTO `pejabat` VALUES ('1', '1', 'MARTIN RANTAN,SH.,M.Sos', 'BUPATI KETAPANG', '1');
INSERT INTO `pejabat` VALUES ('2', '196211241988101002', 'H.FARHAN,SE.,M.Si', 'WAKIL BUPATI KETAPANG', '1');
INSERT INTO `pejabat` VALUES ('3', '197908021998021001', 'ALEXANDER, WILYO,S.STP.,M.Si', 'SEKRETARIS DAERAH KETAPANG', '1');
INSERT INTO `pejabat` VALUES ('4', '197006172000031001', 'EDI RADIANSYAH,SH.,MH', 'ASISTEN I (ASISTEN SEKDA BID. PEMERINTAHAN DAN KESRA)', '0');
INSERT INTO `pejabat` VALUES ('5', '196807231989081003', 'SYAMSUL ISLAMI,S.IP.,MT', 'ASISTEN II (ASISTEN SEKDA BID.EKONOMI DAN PEMBANGUNAN)', '0');
INSERT INTO `pejabat` VALUES ('6', '196307191996031003', 'Drs. HERONIMUS TANAM,ME', 'ASISTEN III (ASISTEN SEKDA BID.ADMINISTRASI DAN UMUM)', '1');
INSERT INTO `pejabat` VALUES ('7', '196306221990031008', 'Drs. JOKO PRASTOWO,MH', 'STAF AHLI BUPATI BID.PEMERINTAHAN,HUKUM DAN POLITIK', '0');
INSERT INTO `pejabat` VALUES ('8', '197205231992021001', 'JUNAIDI FIRRAWAN,S.Sos.,ME', 'STAF AHLI BUPATI BID. KEUANGAN DAN PEMBANGUNAN', '0');
INSERT INTO `pejabat` VALUES ('9', '196911021989031002', 'Drs. H.MARYADI ASMU\'IE,MM', 'STAF AHLI BUPATI BID. KEMASYARAKATAN DAN SDM', '0');

-- ----------------------------
-- Table structure for `protokoler`
-- ----------------------------
DROP TABLE IF EXISTS `protokoler`;
CREATE TABLE `protokoler` (
  `id_protokoler` int(11) NOT NULL AUTO_INCREMENT,
  `id_surat_opd` varchar(50) NOT NULL,
  `tujuan` char(50) NOT NULL,
  `note_protokoler` varchar(200) DEFAULT NULL,
  `id_login` int(11) NOT NULL,
  PRIMARY KEY (`id_protokoler`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of protokoler
-- ----------------------------
INSERT INTO `protokoler` VALUES ('49', '83', '196211241988101002', '', '6');

-- ----------------------------
-- Table structure for `surat_opd`
-- ----------------------------
DROP TABLE IF EXISTS `surat_opd`;
CREATE TABLE `surat_opd` (
  `id_surat_opd` int(11) NOT NULL AUTO_INCREMENT,
  `batch` char(20) NOT NULL,
  `kode_surat` varchar(50) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `asal_surat` varchar(100) NOT NULL,
  `unit_kerja` varchar(100) NOT NULL,
  `tgl_kegiatan` date NOT NULL,
  `waktu` time NOT NULL,
  `lokasi` char(100) NOT NULL,
  `pejabat` int(11) NOT NULL,
  `note_opd` char(100) NOT NULL,
  `daf_undangan` varchar(20) NOT NULL,
  `daf_surat` varchar(20) NOT NULL,
  `daf_sambutan` varchar(20) NOT NULL,
  `status` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `id_login` int(11) NOT NULL,
  `id_opd` varchar(30) NOT NULL,
  PRIMARY KEY (`id_surat_opd`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of surat_opd
-- ----------------------------
INSERT INTO `surat_opd` VALUES ('83', '1658301379001', '111/AA/2022', 'surat undangan', 'bapeda', 'bapeda', '2022-07-20', '08:00:00', 'kantor bupati', '1', '', '1658301379001', '1658301379001', '1658301379001', '4', 'Hadir', '14', '197004111990031004');

-- ----------------------------
-- Table structure for `tbl_adm`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_adm`;
CREATE TABLE `tbl_adm` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengguna` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(12) NOT NULL,
  `id_level` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_adm
-- ----------------------------
INSERT INTO `tbl_adm` VALUES ('1', 'development', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'superadmin', '99');
INSERT INTO `tbl_adm` VALUES ('2', '', 'adm', '21232f297a57a5a743894a0e4a801fc3', 'admin', '0');
INSERT INTO `tbl_adm` VALUES ('3', '198509132003121002', 'prokopim', '21232f297a57a5a743894a0e4a801fc3', 'protokol', '1');
INSERT INTO `tbl_adm` VALUES ('5', '1', 'BUPATI', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('6', '196211241988101002', 'WAKIL', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('7', '197908021998021001', 'SEKDA', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('8', '', 'AS1', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('9', '', 'AS2', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('10', '196307191996031003', 'AS3', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('11', '', 'STAFAHLI1', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('12', '', 'STAFAHLI2', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('13', '', 'STAFAHLI3', '21232f297a57a5a743894a0e4a801fc3', 'pejabat', '2');
INSERT INTO `tbl_adm` VALUES ('14', '197004111990031004', 'BAPEDA', '21232f297a57a5a743894a0e4a801fc3', 'opd', '3');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `ids` int(11) NOT NULL AUTO_INCREMENT,
  `id_pengguna` varchar(20) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `level_dapil` int(11) NOT NULL,
  `telepon` varchar(255) NOT NULL,
  `id_profil` int(11) NOT NULL,
  PRIMARY KEY (`ids`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
