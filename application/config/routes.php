<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login/index';
$route['administrator'] = 'login/aksi_login';
$route['dashboard'] = 'admin/index';
$route['datadiri/(:num)'] = 'login/dataprofil/$1';
$route['logout'] = 'login/logout';

$route['pengguna'] = 'userz/allpengguna';
$route['tambahpengguna'] = 'userz/tambah';
$route['ubahdata/pengguna/(:num)'] = 'userz/edit/$1';
$route['hapusdata/pengguna/(:num)'] = 'userz/hapus/$1';

$route['userfile'] = 'pengguna/datapengguna';

$route['opdview'] = 'surat_opd/tampil';
$route['opdentry'] = 'surat_opd/tambah';
$route['hapusdata/surat_opd/(:num)'] = 'surat_opd/hapus/$1';
$route['ubahdata/surat_opd/(:num)'] = 'surat_opd/edit/$1';
$route['ubahdata/viewPdf/(:num)'] = 'surat_opd/viewPdf/$1';

$route['textview'] = 'auto_text/tampil';
$route['textentry'] = 'auto_text/tambah';
$route['hapusdata/auto_text/(:num)'] = 'auto_text/hapus/$1';
$route['ubahdata/auto_text/(:num)'] = 'auto_text/edit/$1';

$route['protokolerview'] = 'protokoler/tampil';
$route['protokolerreview'] = 'protokoler/publish';
$route['protokolerentry'] = 'protokoler/tambah';
$route['hapusdata/protokoler/(:num)'] = 'protokoler/hapus/$1';
$route['ubahdata/protokoler/(:num)'] = 'protokoler/edit/$1';

$route['disposisiview'] = 'disposisi/tampil';
$route['disposisientry'] = 'disposisi/tambah';
$route['hapusdata/disposisi/(:num)'] = 'disposisi/hapus/$1';
$route['ubahdata/reschedule/(:num)'] = 'disposisi/edit_jadwal/$1';
$route['ubahdata/disposisikan/(:num)'] = 'disposisi/disposisikan/$1';
$route['ubahdata/hadir/(:num)'] = 'disposisi/upstatus/$1';

$route['log-out'] = 'login/logout';
$route['log-in'] = 'login/index';
//$route['(.*)'] = "error404";
$route['translate_uri_dashes'] = FALSE;
