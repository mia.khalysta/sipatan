<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Protokoler extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('Model_protokoler');
	$this->load->model('Model_surat_opd');
    $this->load->helper(array('form', 'url'));
	$this->load->library('form_validation');
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
  }

  public function index()
  {

    $this->load->view('index');
  }
  
  public function tampil()
  {

    $data = array(
      'title' => 'Agenda Acara',
      'data_protokoler' => $this->Model_protokoler->get_surat_opd(),
	  'jns_pejabat' => $this->Model_surat_opd->get_pejabat(),
      'isi' => 'backend/protokoler/data_tampil'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  public function publish()
  {

    $data = array(
      'title' => 'Agenda Acara',
      'data_protokoler' => $this->Model_protokoler->get_surat_opd(),
	  'jns_pejabat' => $this->Model_surat_opd->get_pejabat(),
      'isi' => 'backend/protokoler/publikasi'
    );
    $this->load->view('backend/layout/wrapper', $data);
  }
  
  public function edit($idjb)
	{
        $idjb = $this->uri->segment(3);
		
		$data = array(
		  'title'     => 'Update Protokoler',
		  'data_jb' => $this->Model_protokoler->surat_opd($idjb),
		  'jns_pejabat' => $this->Model_protokoler->get_ditujukan(),
		  'isi' => 'backend/protokoler/perubahan_surat'
		);
			
		$this->form_validation->set_rules('txtkodesurat', 'Kode Surat', 'required');
        $this->form_validation->set_rules('txtperihal', 'Perihal', 'required');
		$this->form_validation->set_rules('txtasal', 'Asal', 'required');
        $this->form_validation->set_rules('txtunit', 'Unit', 'required');
		$this->form_validation->set_rules('txttgl', 'Tanggal', 'required');
        $this->form_validation->set_rules('txtwaktu', 'Waktu', 'required');
		$this->form_validation->set_rules('txtlokasi', 'Lokasi', 'required');
        $this->form_validation->set_rules('txtpejabat', 'Dikemukakan Kepada', 'required');
		$this->form_validation->set_rules('txtnote', 'Note', 'required');
		$this->form_validation->set_rules('cover', 'cover', 'required');
		$this->form_validation->set_rules('catalog', 'catalog', 'required');
		$this->form_validation->set_rules('draft', 'draft', 'required');

        if ($this->form_validation->run() == false) {
            //GAGAL
            
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
		//$kunci['kunci']=strtotime(date('Y-m-d H:i:s'));
		//$this->Model_protokoler->simpan($kunci);
    }

  public function update()
  {
    $id['id_protokoler'] = $this->input->post("TxtIDprotokoler");
	
    $data = array(
		'kode_surat' => htmlspecialchars($this->input->post("txtkodesurat", true)),
		'perihal' => htmlspecialchars($this->input->post("txtperihal", true)),
		'asal_surat' => htmlspecialchars($this->input->post("txtasal", true)),
		'unit_kerja' => htmlspecialchars($this->input->post("txtunit", true)),
		'tgl_kegiatan' => htmlspecialchars($this->input->post("txttgl", true)),
		'waktu' => htmlspecialchars($this->input->post("txtwaktu", true)),
		'lokasi' => htmlspecialchars($this->input->post("txtlokasi", true)),
		'pejabat' => htmlspecialchars($this->input->post("txtpejabat", true)),
		'note_protokoler' => htmlspecialchars($this->input->post("txtnote", true))
        );

    $this->Model_protokoler->update($data, $id);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Update</div>');

    redirect('protokolerview');
  }

/*  public function tambah()
  {
    $data = array(
	  'title' => 'Input Surat',
	  'data_jb' => $this->Model_protokoler->get_all(),
	  'jns_pejabat' => $this->Model_surat_opd->get_pejabat(),
	  'isi' => 'backend/protokoler/tambah_surat'
	);
	$this->form_validation->set_rules('txtkodesurat', 'Kode Surat', 'required');
	$this->form_validation->set_rules('txtperihal', 'Perihal', 'required');
	$this->form_validation->set_rules('txtasal', 'Asal', 'required');
	$this->form_validation->set_rules('txtunit', 'Unit', 'required');
	$this->form_validation->set_rules('txttgl', 'Tanggal', 'required');
	$this->form_validation->set_rules('txtwaktu', 'Waktu', 'required');
	$this->form_validation->set_rules('txtlokasi', 'Lokasi', 'required');
	$this->form_validation->set_rules('txtpejabat', 'Dikemukakan Kepada', 'required');
	$this->form_validation->set_rules('txtnote', 'Note', 'required');
	$this->form_validation->set_rules('cover', 'cover', 'required');
	$this->form_validation->set_rules('catalog', 'catalog', 'required');
	$this->form_validation->set_rules('draft', 'draft', 'required');
		
		if ($this->form_validation->run() == false) {
            //GAGAL
            
			$this->load->view('backend/layout/wrapper', $data);
        } else {
            //BERHASIL
            $this->simpan();
        }
	
  }
  public function hapus($idj)
  {
    $id['id_protokoler'] = $this->uri->segment(3);
    $this->Model_protokoler->hapus($id);
    redirect('protokolerview');
  }
*/  
  public function simpan()
  {
    $id = $this->input->post("TxtIDsuratopd");
	
	$data = array(
	  'id_login' => $this->session->userdata("id"),
      'id_surat_opd' => htmlspecialchars($this->input->post("TxtIDsuratopd", true)),
	  'tujuan' => htmlspecialchars($this->input->post("txttujuan", true)),
	  'note_protokoler' => htmlspecialchars($this->input->post("txtnote", true))
    );
	
	$datax = array(
	  'id_login' => $this->session->userdata("id"),
      'surat_opd' => htmlspecialchars($this->input->post("TxtIDsuratopd", true)),
	  'disposisikan' => htmlspecialchars($this->input->post("txttujuan", true)),
	  'note_disposisi' => htmlspecialchars($this->input->post("txtnote", true))
    );
	$this->db->insert('disposisi',$datax);
	
	$update = $this->db->query("UPDATE surat_opd SET status= '2', keterangan= 'Disposisi' WHERE surat_opd.id_surat_opd = '$id'");
    $this->Model_protokoler->simpan($data);
    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Success! Data Berhasil di Simpan</div>');

    redirect('protokolerview');
  
  }

  

} // END OF class kecamatan
