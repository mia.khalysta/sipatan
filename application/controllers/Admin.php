<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->load->model('m_login');
		//$this->load->model('model_suara');
		$this->load->helper(array('form', 'url'));
        if ($this->session->userdata('status') != "loginCOD") {
            redirect(base_url("log-in"));
        }
    }

	public function index()
	{
		if ($this->session->userdata('level') == "superadmin") {
		$data = array(
            'title' => 'SIPATAN',
            'isi' => 'backend/home',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}elseif($this->session->userdata('level') == "opd") {
		$data = array(
            'title' => 'SIPATAN',
            'isi' => 'backend/home',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}elseif($this->session->userdata('level') == "protokol") {
		$data = array(
            'title' => 'SIPATAN',
            'isi' => 'backend/home',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}elseif($this->session->userdata('level') == "pejabat") {
		$data = array(
            'title' => 'SIPATAN',
            'isi' => 'backend/home',
        );
		$this->load->view('backend/layout/wrapper', $data);
		
		}else{
		$this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible" role="alert">Username atau Password Anda Tidak Ditemukan</div>');
		
		redirect(base_url("login"));
		}
	}
}