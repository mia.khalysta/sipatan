<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_protokoler extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('protokoler')
      ->order_by('id_protokoler', 'ASC')
      ->get();
    return $query->result();
  }

  public function get_ditujukan()
  {
    $query = $this->db->query("SELECT * FROM pejabat WHERE ditujukan='1' ");
    return $query->result();
  }
  
  public function get_pejabat()
  {
    $query = $this->db->select("*")
      ->from('pejabat')
      ->order_by('id_pejabat', 'ASC')
      ->get();
    return $query->result();
  }
  
  public function simpan($data)
  {
    $query = $this->db->insert("protokoler", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("protokoler", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
    
  public function get_surat_opd()
  {
	$query = $this->db->query("SELECT * FROM surat_opd");
    return $query->result();
  }
  
  public function surat_opd($idjb)
  {
    $query = $this->db->where("id_surat_opd", $idjb)
      ->get("surat_opd");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }
  
  public function edit($idjb)
  {
    $query = $this->db->where("id_protokoler", $idjb)
      ->get("protokoler");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("protokoler", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
} // END OF class Model_protokoler
