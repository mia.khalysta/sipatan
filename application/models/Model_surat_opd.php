<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Model_surat_opd extends CI_model
{

  public function get_all()
  {
    $query = $this->db->select("*")
      ->from('surat_opd')
      ->order_by('id_surat_opd', 'ASC')
      ->get();
    return $query->result();
  }
	
  public function get_pejabat()
  {
    $query = $this->db->select("*")
      ->from('pejabat')
      ->order_by('id_pejabat', 'ASC')
      ->get();
    return $query->result();
  }
	
  public function simpan($data)
  {
    $query = $this->db->insert("surat_opd", $data);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }

  public function hapus($id)
  {
    $query = $this->db->delete("surat_opd", $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
  public function edit($idjb)
  {
    $query = $this->db->where("id_surat_opd", $idjb)
      ->get("surat_opd");
    if ($query) {
      return $query->row();
    } else {
      return false;
    }
  }

  public function update($data, $id)
  {
    $query = $this->db->update("surat_opd", $data, $id);

    if ($query) {
      return true;
    } else {
      return false;
    }
  }
  
  public function CreateCode(){
	  $this->db->select('RIGHT(surat_opd.id_surat_opd,2) as kode_kunci', FALSE);
	  $this->db->order_by('kode_kunci','DESC');    
	  $this->db->limit(1);    
	  $query = $this->db->get('surat_opd');  //cek dulu apakah sudah ada kode di tabel.    
	  if($query->num_rows() <> 0){      
		   //cek kode jika telah tersedia    
		   $data = $query->row();      
		   $kode = intval($data->kode_kunci) + 1; 
	  }
	  else{      
		   $kode = 1;  //cek jika kode belum terdapat pada table
	  }
		  $tgl=strtotime(date("Y-m-d H:i:s")); 
		  $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);    
		  //$kodetampil = "BR"."5".$tgl.$batas;  //format kode
		  $kodetampil = $tgl.$batas;  //format kode
		  return $kodetampil;  
  }
} // END OF class Model_surat_opd
