<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $title ?></h4>
				  <?php echo $this->session->flashdata('notif') ?>
						<?php echo form_open_multipart('auto_text/simpan') ?>
				  <form class="forms-sample">
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Isi Text</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <textarea type="text" class="form-control" name="txtisi" required  value="<?= set_value('txtisi'); ?>"></textarea>
                      </div>
                    </div>
                    <hr><button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button type="reset" class="btn btn-md btn-warning">reset</button>
					<a href="<?php echo base_url() ?>textview" type="button" class="btn btn-md btn-danger"><span ></span> Kembali</a>
					<?php echo form_close() ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->