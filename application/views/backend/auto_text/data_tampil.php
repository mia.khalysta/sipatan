<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $title ?></h4>
				  <?php echo $this->session->flashdata('notif') ?>
                  <div class="row">
                    <div class="col-12">
					  <div class="table-responsive">
						<table id="order-listing" class="table table-striped">
						  <thead>
							<tr class="bg-primary text-white">
								<th>No</th>
								<th>Isi Auto Text</th>
								<th>Actions</th>
							</tr>
						  </thead>
						  <tbody>
						  <?php
							$no = 1;
							foreach ($data_auto_text as $hasil) {
						  ?>
							<tr>
								<td><?php echo $no++ ?></td>								
								<td><?php echo $hasil->isi_text ?></td>
								<td>
									<a href="<?php echo base_url() ?>ubahdata/auto_text/<?php echo $hasil->id_auto_text ?>" class="badge badge-success">Edit</a>
									<a href="<?php echo base_url() ?>hapusdata/auto_text/<?php echo $hasil->id_auto_text ?>" class="badge badge-danger" onclick="return confirm('Yakin ingin menghapus ?')">Hapus</a>
								</td>
							</tr>
						  <?php } ?>
						  </tbody>
						</table>
					  </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->