<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $title ?></h4>
				  <?php echo $this->session->flashdata('notif') ?>
						<?php echo form_open_multipart('surat_opd/simpan') ?>

                  
				  <form class="forms-sample">
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Kode/Nomor Surat</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="text" class="form-control" name="txtkodesurat" required  value="<?= set_value('txtkodesurat'); ?>">
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Perihal</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="text" class="form-control" name="txtperihal" required  value="<?= set_value('Txtkodebidang'); ?>">
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Asal Surat</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="text" class="form-control" name="txtasal" required  value="<?= set_value('txtasal'); ?>">
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Unit Kerja</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="text" class="form-control" name="txtunit" required  value="<?= set_value('txtunit'); ?>">
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Tanggal</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="date" class="form-control" name="txttgl" required  value="<?= set_value('txttgl'); ?>">
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Waktu</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="time" class="form-control" name="txtwaktu" required  value="<?= set_value('txtwaktu'); ?>">
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Lokasi</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="text" class="form-control" name="txtlokasi" required  value="<?= set_value('txtlokasi'); ?>">
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Dikemukakan Kepada</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">						
						<select class="js-example-basic-single w-100" name="txtpejabat" required  value="<?= set_value('txtpejabat'); ?>">
							<option value="" disabled selected>--Pilih--</option>
							<?php
							foreach ($jns_pejabat as $Dtpejabat) {
								?>
								<option value="<?= $Dtpejabat->id_pejabat ?>"><?= $Dtpejabat->jabatan ?></option>
							<?php
							}
							?>
						</select>								
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Upload Undangan</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="file" name="cover" class="file-upload-default" accept=".pdf" required  value="<?= set_value('cover'); ?>">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="*Type File yang di upload dalam bentuk PDF">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                      </div>
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Lampiran Surat</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="file" name="catalog" class="file-upload-default" accept=".pdf" required  value="<?= set_value('catalog'); ?>">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="*Type File yang di upload dalam bentuk PDF">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                      </div>
                      </div>
                    </div>
					<div class="form-group row" style="margin-bottom: 0rem;">
                      <label class="col-sm-3 col-form-label">Lampiran Sambutan</label>
                      <div class="col-sm-9"><input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="file" name="draft" class="file-upload-default" accept=".pdf" required  value="<?= set_value('draft'); ?>">
                      <div class="input-group col-xs-12">
                        <input type="text" class="form-control file-upload-info" disabled placeholder="*Type File yang di upload dalam bentuk PDF">
                        <span class="input-group-append">
                          <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                        </span>
                      </div>
                      </div>
                    </div>
					
                    <hr><button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button type="reset" class="btn btn-md btn-warning">reset</button>
					<a href="<?php echo base_url() ?>opdview" type="button" class="btn btn-md btn-danger"><span ></span> Kembali</a>
					<?php echo form_close() ?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->