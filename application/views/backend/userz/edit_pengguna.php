<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">
	<div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

	<div class="container-fluid">
        <div class="row">
            <div class="col-md-12 card">
			<div class="card-body">
                <?php echo $this->session->flashdata('notif') ?>                
                <?php echo form_open_multipart('pengguna/update') ?>			
					<div class="col-md-6" style="float: left;">				
						<div class="form-group">
						<input type="hidden" name="id" value="<?php echo $data_p->id_pengguna ?>">
							<label for="text">Nama Karyawan</label>
							<input type="text" name="nama" class="form-control" value="<?php echo $data_x->nama ?>" required autofocus>
						</div>
						<div class="form-group">
							<label for="text">No. HP</label>
							<input type="text" name="telp" class="form-control" value="<?php echo $data_x->telepon ?>" required>
						</div>
					</div>
					<div class="col-md-6" style="float: left;">
						<div class="form-group">
							<label for="text">Username</label>
							<input type="text" name="username" class="form-control" value="<?php echo $data_p->username ?>" required>
						</div>
						<div class="form-group">
							<label for="text">Password</label>
							<input type="password" name="pass" class="form-control" placeholder="Masukkan Password Baru" required>
						</div>
					</div>
                <button type="submit" class="btn btn-md btn-success">Update</button>
                <button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
                <?php echo form_close() ?>
            </div>
			</div>
		</div>
	</div>
</div>	