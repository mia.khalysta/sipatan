<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="page-wrapper">
	<div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title"><?php echo $title ?></h4>
            </div>
        </div>
    </div>

    <div class="container-fluid">

        <div class="row">
            <div class="col-12">

                <div class="card">
                    <div class="card-body">
                        <?php echo $this->session->flashdata('notif') ?>
                        <a href="<?php echo base_url() ?>login/tambah" class="btn btn-md btn-success">Tambah Profil</a>
                        <hr>

                        <div class="table-responsive">
                            <table id="zero_config" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Level</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $no = 1;
                                    foreach ($data_profil as $hasil) {
                                        ?>

                                        <tr>
                                            <td><?php echo $no++ ?></td>
                                            <td><?php echo $hasil->nama ?></td>
                                            <td><?php echo $hasil->username ?></td>
                                            <td><?php
                                                    if (($hasil->level) == 1) {
                                                        echo 'Master Admin';
                                                    } else {
                                                        echo 'Admin';
                                                    } ?></td>
                                            <td>
                                                <a href="<?php echo base_url() ?>login/edit/<?php echo $hasil->id_user ?>" class="btn btn-sm btn-success">Edit</a>
                                                <?php
                                                    if (($hasil->level) == 1) {
                                                        ?>
                                                    <a href="#" class="btn btn-sm btn-default" style="background-color: gainsboro;">Hapus</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo base_url() ?>login/hapus/<?php echo $hasil->id_user ?>" class="btn btn-sm btn-danger" onclick="return confirm('Yakin ingin dihapus ?')">Hapus</a>
                                                <?php } ?>
                                            </td>
                                        </tr>

                                    <?php } ?>

                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>