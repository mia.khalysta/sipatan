<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ($this->session->userdata('id') == "10") {
?>
<div class="main-panel">
	<div class="content-wrapper">
	  <div class="row">


		<div class="col-12 grid-margin stretch-card">
		  <div class="card">
			<div class="card-body">
			  <h4 class="card-title"><?php echo $title ?></h4>
			<?php echo $this->session->flashdata('notif') ?>
			<?php echo form_open_multipart('disposisi/simpan') ?>
			<input type="hidden" name="TxtIDsuratopd" value="<?= $data_jb->id_surat_opd ?>">
			
			  <form class="forms-sample">			  
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Perihal</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtkodesurat" required  value="<?= $data_jb->kode_surat ?> - <?= $data_jb->perihal ?>" readonly >
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Pelaksanaan</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtperihal" required  value="<?= $data_jb->tgl_kegiatan ?> - <?= $data_jb->waktu ?>" readonly >
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Disposisikan Kepada</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<select class="form-control" name="txtdisposisi">
						<option disabled selected>--Pilih--</option>
						<?php
						  foreach ($jns_as3 as $Dtpejabat) {
						?>
						<option value="<?= $Dtpejabat->nip ?>" 
						<?php 
						if(($this->session->userdata("id_pengguna")) == ($Dtpejabat->nip)) {
							echo 'hidden'; } ?> >
							<?= $Dtpejabat->jabatan; ?>
						</option>						
						<?php
						  }
						?>
					</select>
				  </div>
				</div>
				<hr><button type="submit" class="btn btn-primary mr-2">Submit</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			  </form>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<!-- content-wrapper ends -->
</div>
<?php
}elseif($this->session->userdata('id') == "5") {
?>
<div class="main-panel">
	<div class="content-wrapper">
	  <div class="row">


		<div class="col-12 grid-margin stretch-card">
		  <div class="card">
			<div class="card-body">
			  <h4 class="card-title"><?php echo $title ?></h4>
			<?php echo $this->session->flashdata('notif') ?>
			<?php echo form_open_multipart('disposisi/simpan') ?>
			<input type="hidden" name="TxtIDsuratopd" value="<?= $data_jb->id_surat_opd ?>">
			
			  <form class="forms-sample">			  
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Perihal</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtkodesurat" required  value="<?= $data_jb->kode_surat ?> - <?= $data_jb->perihal ?>" readonly >
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Pelaksanaan</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtperihal" required  value="<?= $data_jb->tgl_kegiatan ?> - <?= $data_jb->waktu ?>" readonly >
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Disposisikan Kepada</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<select class="form-control" name="txtdisposisi">
						<option disabled selected>--Pilih--</option>
						<?php
						  foreach ($jns_pejabat as $Dtpejabat) {
						?>
						<option value="<?= $Dtpejabat->nip ?>" 
						<?php 
						if(($this->session->userdata("id_pengguna")) == ($Dtpejabat->nip)) {
							echo 'hidden'; } ?> >
							<?= $Dtpejabat->jabatan; ?>
						</option>						
						<?php
						  }
						?>
					</select>
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Note</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<textarea class="form-control" rows="4" name="txtnote"></textarea>
				  </div>
				</div>
				<hr><button type="submit" class="btn btn-primary mr-2">Submit</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			  </form>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<!-- content-wrapper ends -->
</div>
<?php
}else{
?>
<div class="main-panel">
	<div class="content-wrapper">
	  <div class="row">


		<div class="col-12 grid-margin stretch-card">
		  <div class="card">
			<div class="card-body">
			  <h4 class="card-title"><?php echo $title ?></h4>
			<?php echo $this->session->flashdata('notif') ?>
			<?php echo form_open_multipart('disposisi/simpan') ?>
			<input type="hidden" name="TxtIDsuratopd" value="<?= $data_jb->id_surat_opd ?>">
			
			  <form class="forms-sample">			  
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Perihal</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtkodesurat" required  value="<?= $data_jb->kode_surat ?> - <?= $data_jb->perihal ?>" readonly >
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Pelaksanaan</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtperihal" required  value="<?= $data_jb->tgl_kegiatan ?> - <?= $data_jb->waktu ?>" readonly >
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Disposisikan Kepada</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<select class="form-control" name="txtdisposisi">
						<option disabled selected>--Pilih--</option>
						<?php
						  foreach ($jns_pejabat as $Dtpejabat) {
						?>
						<option value="<?= $Dtpejabat->nip ?>" 
						<?php 
						if(($this->session->userdata("id_pengguna")) == ($Dtpejabat->nip)) {
							echo 'hidden'; } ?> >
							<?= $Dtpejabat->jabatan; ?>
						</option>						
						<?php
						  }
						?>
					</select>
				  </div>
				</div>
				<hr><button type="submit" class="btn btn-primary mr-2">Submit</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			  </form>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<!-- content-wrapper ends -->
</div>
<?php
}
?>