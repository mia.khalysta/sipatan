<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="main-panel">
	<div class="content-wrapper">
	  <div class="row">


		<div class="col-12 grid-margin stretch-card">
		  <div class="card">
			<div class="card-body">
			  <h4 class="card-title"><?php echo $title ?></h4>
			<?php echo $this->session->flashdata('notif') ?>
			<?php echo form_open_multipart('disposisi/update') ?>
			<input type="hidden" name="TxtIDsuratopd" value="<?= $data_jb->id_surat_opd ?>">
			
			  <form class="forms-sample">			  
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Kode/Nomor Surat</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtkodesurat" required  value="<?= $data_jb->kode_surat ?>" readonly>
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Perihal</label>
				  <div class="col-sm-9">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtperihal" required  value="<?= $data_jb->perihal ?>" readonly>
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Asal Surat</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtasal" required  value="<?= $data_jb->asal_surat ?>" readonly>
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Unit Kerja</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtunit" required  value="<?= $data_jb->unit_kerja ?>" readonly>
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Tanggal</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="date" class="form-control" name="txttgl" required  value="<?= $data_jb->tgl_kegiatan ?>" >
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Waktu</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="time" class="form-control" name="txtwaktu" required  value="<?= $data_jb->waktu ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Lokasi</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<input type="text" class="form-control" name="txtlokasi" required  value="<?= $data_jb->lokasi ?>">
				  </div>
				</div>
				<div class="form-group row" style="margin-bottom: 0rem;">
				  <label class="col-sm-3 col-form-label">Note</label>
				  <div class="col-sm-9">
				  <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<textarea class="form-control" rows="4" name="txtnote"></textarea>
				  </div>
				</div>

				<hr><button type="submit" class="btn btn-primary mr-2">Submit</button>
				<button type="reset" class="btn btn-md btn-warning">reset</button>
				<button type="button" class="btn btn-md btn-danger" onclick="javascript:history.back()"><span ></span> Batal</button>
				<?php echo form_close() ?>
			  </form>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<!-- content-wrapper ends -->
</div>