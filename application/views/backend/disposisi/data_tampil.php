<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">


            <div class="col-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $title ?></h4>
				  <?php echo $this->session->flashdata('notif') ?>
                  <div class="row">
                    <div class="col-12">
					  <div class="table-responsive">
						<table id="order-listing" class="table">
						  <thead>
							<tr class="bg-primary text-white">
								<th>No</th>
								<th>Tanggal</th>
								<th>Perihal</th>
								<th>Disposisi</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						  </thead>
						  <tbody>
						  <?php
							$no = 1;
							foreach ($data_protokoler as $hasil) {
							if(
							($this->session->userdata("id_pengguna")) == ($hasil->tujuan) AND
							($hasil->status)== "2"	
							){
								
						  ?>
							<tr>
								<td><?php echo $no++ ?></td>
								<td><?php echo $hasil->tgl_kegiatan ?></td>
								<td><?php echo $hasil->perihal ?></td>
								<td>
									<?php foreach ($jns_pejabat as $stat) {
									if (($hasil->tujuan) == ($stat->id_pejabat)) {
									 echo $stat->jabatan; }
									}?>
								</td>
								<td><?php echo $hasil->keterangan ?></td>								
								<td>  
								  <a href="<?php echo base_url() ?>ubahdata/disposisikan/<?php echo $hasil->idsuratopd ?>" class="badge badge-info">Disposisi</a>
								  <a href="<?php echo base_url() ?>ubahdata/reschedule/<?php echo $hasil->idsuratopd ?>" class="badge badge-warning">Reschedule</a>
								  <a href="<?php echo base_url() ?>ubahdata/hadir/<?php echo $hasil->idsuratopd ?>" class="badge badge-success">Hadir</a>
                                </td>
							</tr>
							<?php }} ?>
						  </tbody>
						</table>
					  </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->

      </div>
      <!-- main-panel ends -->